[
  {
    "animeId": 1,
    "animeTitle": "Tonikawa:2nd Season",
    "animeDesc": "A Young Prodigy accepts a Sudden Proposal from a Mysterious Woman who he falls in love with after she saves him from a Near-Death Experience.",
    "animeImage": "/assets/images/spotlight1.jpg",
    "animeSection": "spotlight",
    "genre": "Romance"
  },
  {
    "animeId": 2,
    "animeTitle": "Dr. Stone: New World",
    "animeDesc": "With the Stone Wars over, the former members of Tsukasa's Empire of Might join forces with the Kingdom of Science to build a ship capable of sailing across open ocean to seek answers on the mystery of the global petrification.",
    "animeImage": "/assets/images/spotlight2.jpg",
    "animeSection": "spotlight",
    "genre": "Shonen"
  },
  {
    "animeId": 3,
    "animeTitle": "The Legendary Hero is Dead",
    "animeDesc": "Foolow the fake hero on his quest to defeat the demon lord at the place of real hero.",
    "animeImage": "/assets/images/spotlight3.jpg",
    "animeSection": "spotlight",
    "genre": "Isekai"
  },
  {
    "animeId": 4,
    "animeTitle": "The Ancient Magus's Bride: Season 2",
    "animeDesc": "The second season of Mahoutsukai no Yome. Chise was able to accept Elias and herself, if not necessarily everything about her situation. After Cartaphilus fell back into a slumber that would not last forever.",
    "animeImage": "/assets/images/spotlight4.jpg",
    "animeSection": "spotlight",
    "genre": "Adventure"
  },
  {
    "animeId": 5,
    "animeTitle": "I Got a Cheat Skill in Another World and Became Unrivaled in The Real World, Too",
    "animeDesc": "A chance to come back! A mysterious door stands open, inviting a boy who's been brutally bullied all his life to take a courageous step forward into the unknown.",
    "animeImage": "/assets/images/spotlight5.jpg",
    "animeSection": "spotlight",
    "genre": "Isekai"
  },
  {
    "animeId": 6,
    "animeTitle": "Skip and Loafer",
    "animeDesc": "Excellent student Iwakura Mitsumi has always dreamt about leaving her small town, going to a prestigious university, and making positive change in the world. But she's so focused on reaching her goals that she's not prepared for the very different.",
    "animeImage": "/assets/images/spotlight6.jpg",
    "animeSection": "spotlight",
    "genre": "Slice of life"
  },
  {
    "animeId": 7,
    "animeTitle": "Hell's Paradise",
    "animeDesc": "Gabimaru the Hollow, a ninja of Iwagakure Village known for being cold and emotionless, was set up by his fellow ninja and is now on death row. Tired of killing and betrayal, he wants to die. However, no method of execution works on him.",
    "animeImage": "/assets/images/spotlight7.jpg",
    "animeSection": "spotlight",
    "genre": "Action"
  },
  {
    "animeId": 8,
    "animeTitle": "Loving Yamada At LV999",
    "animeDesc": "After her boyfriend cheats on her with another girl he met in-game, Akane Kinoshita learns the hard way that gamer boyfriends can be just as bad as the offline variety. But later, she has a chance encounter with Akito Yamada.",
    "animeImage": "/assets/images/spotlight8.jpg",
    "animeSection": "spotlight",
    "genre": "Romance"
  },
  {
    "animeId": 9,
    "animeTitle": "Eden Zero: 2nd Season",
    "animeDesc": "At Granbell Kingdom, an abandoned amusement park, Shiki has lived his entire life among machines. But one day, Rebecca and her cat companion Happy appear at the park's front gates and they embark on a new adventure.",
    "animeImage": "/assets/images/spotlight9.jpg",
    "animeSection": "spotlight",
    "genre": "Adventure"
  },
  {
    "animeId": 10,
    "animeTitle": "Bleach",
    "animeDesc": "Ichigo Kurosaki is an ordinary high schooler—until his family is attacked by a Hollow, a corrupt spirit that seeks to devour human souls. It is then that he meets a Soul Reaper named Rukia Kuchiki.",
    "animeImage": "/assets/images/spotlight10.jpg",
    "animeSection": "spotlight",
    "genre": "Shounen"
  },
  {
    "animeId": 11,
    "animeTitle": "One Piece",
    "animeDesc": "Gold Roger was known as the Pirate King, the strongest and most infamous being to have sailed the Grand Line. The capture and execution of Roger by the World Government brought a change throughout the world.",
    "animeImage": "/assets/images/trending01.jpg",
    "animeSection": "trending",
    "genre": "Shounen"
  },
  {
    "animeId": 12,
    "animeTitle": "Demon Slayer : Season 3",
    "animeDesc": "To explain to Hotaru Haganezuk, the master swordsmith who forged Tanjiro's sword, why his sword was so severely injured, Tanjiro had to travel to a swordsmith village.",
    "animeImage": "/assets/images/trending2.jpg",
    "animeSection": "trending",
    "genre": "Shounen"
  },
  {
    "animeId": 13,
    "animeTitle": "Oshi No Ko",
    "animeDesc": "Sixteen-year-old Ai Hoshino is a talented and beautiful idol who is adored by her fans. She is the personification of a pure, young maiden. But all that glitters is not gold.",
    "animeImage": "/assets/images/trending3.jpg",
    "animeSection": "trending",
    "genre": "Drama"
  },
  {
    "animeId": 14,
    "animeTitle": "I Got a Cheat Skill in Another World and Became Unrivaled in The Real World, Too",
    "animeDesc": "A chance to come back! A mysterious door stands open, inviting a boy who's been brutally bullied all his life to take a courageous step forward into the unknown.",
    "animeImage": "/assets/images/trending4.jpg",
    "animeSection": "trending",
    "genre": "Isekai"
  },
  {
    "animeId": 15,
    "animeTitle": "Mashle: Magic and Muscles",
    "animeDesc": "To everyone else in his magic-dominated world, the young and powerless Mash Burnedead is a threat to the gene pool and must be purged.",
    "animeImage": "/assets/images/trending5.jpg",
    "animeSection": "trending",
    "genre": "Comedy"
  },
  {
    "animeId": 16,
    "animeTitle": "Hell's Paradise",
    "animeDesc": "Gabimaru the Hollow, a ninja of Iwagakure Village known for being cold and emotionless, was set up by his fellow ninja and is now on death row. Tired of killing and betrayal, he wants to die.",
    "animeImage": "/assets/images/trending6.jpg",
    "animeSection": "trending",
    "genre": "Action"
  },
  {
    "animeId": 17,
    "animeTitle": "Dr. Stone: New World",
    "animeDesc": "With the Stone Wars over, the former members of Tsukasa's Empire of Might join forces with the Kingdom of Science to build a ship capable of sailing across open ocean to seek answers.",
    "animeImage": "/assets/images/trending7.jpg",
    "animeSection": "trending",
    "genre": "Adventure"
  },
  {
    "animeId": 18,
    "animeTitle": "Blue Lock",
    "animeDesc": "Swinging between his ideals and his ego, Isagi crushes the opponents to become the best striker.",
    "animeImage": "/assets/images/trending8.jpg",
    "animeSection": "trending",
    "genre": "Sports"
  },
  {
    "animeId": 19,
    "animeTitle": "My Hero Academia Season 6",
    "animeDesc": "Season 6 of MHA, once again deku is against the Shigaraki and his minions.",
    "animeImage": "/assets/images/trending9.jpg",
    "animeSection": "trending",
    "genre": "Shounen"
  },
  {
    "animeId": 20,
    "animeTitle": "Tokyo Revengers: Christmas Showdown",
    "animeDesc": "Christmas Showdown arc of Tokyo Revengers",
    "animeImage": "/assets/images/trending10.jpg",
    "animeSection": "trending",
    "genre": "Shounen"
  },
  {
    "animeId": 21,
    "animeTitle": "Mix: Meisei Story 2nd Season",
    "animeDesc": "Because of the legend left by Tatsuya Uesugi, Meisei Academy High School was well-known for their strong baseball team. But 26 years after their glory, the team has not been able to keep their record and has since lost their fame.",
    "animeImage": "/assets/images/l1.jpg",
    "animeSection": "latest",
    "genre": "Sports"
  },
  {
    "animeId": 22,
    "animeTitle": "Pokémon Horizons: The Series",
    "animeDesc": "Follow Liko and Roy as they unravel the mysteries that surround them and encounter Friede, Captain Pikachu, Amethio, and others during their exciting adventures!",
    "animeImage": "/assets/images/l2.jpg",
    "animeSection": "latest",
    "genre": "Adventure"
  },
  {
    "animeId": 23,
    "animeTitle": "Magical Girl Magical Destroyers",
    "animeDesc": "Set in a dystopic near future where otaku culture in Japan has been obliterated by a mysterious organization known as SSC, Magical Girl Destroyers follows the misadventures of Otaku Hero.",
    "animeImage": "/assets/images/l3.jpg",
    "animeSection": "latest",
    "genre": "Fantasy"
  },
  {
    "animeId": 24,
    "animeTitle": "Birdie Wing: Golf Girls' Story: 2nd Season",
    "animeDesc": "is a youth story centering on two female golfers. Two girls, Eve and Aoi, who grew up in completely different circumstances and have different play styles, are caught up in the world of women in golf.",
    "animeImage": "/assets/images/l4.jpg",
    "animeSection": "latest",
    "genre": "Sports"
  },
  {
    "animeId": 25,
    "animeTitle": "Otaku Elf",
    "animeDesc": "Koito Koganei works as the teenage shrine maiden at the Takamimi Shrine, catering to the whims of its resident: a centuries-old elf who loves video games as much as she hates going outside!",
    "animeImage": "/assets/images/l5.jpg",
    "animeSection": "latest",
    "genre": "Comedy"
  },
  {
    "animeId": 26,
    "animeTitle": "Rokudo's Bad Girls",
    "animeDesc": "Tousuke Rokudou is a first-year attending Aomori High, a school full of delinquents. He is often bullied by his classmates due to his incompatibility with their behavior, the only exceptions being his two good friends.",
    "animeImage": "/assets/images/l6.jpg",
    "animeSection": "latest",
    "genre": "Comedy"
  },
  {
    "animeId": 27,
    "animeTitle": "Mashle: Magic and Muscles",
    "animeDesc": "To everyone else in his magic-dominated world, the young and powerless Mash Burnedead is a threat to the gene pool and must be purged. Living secretly in the forest.",
    "animeImage": "/assets/images/l7.jpg",
    "animeSection": "latest",
    "genre": "Comedy"
  },
  {
    "animeId": 28,
    "animeTitle": "Too Cute Crisis",
    "animeDesc": "Liza Luna is an alien who wants to destroy Earth upon her first look. But the planet has a plethora of good things, and it would be a pity for her not to check it out. Thus, Earth is spared from destruction.",
    "animeImage": "/assets/images/l8.jpg",
    "animeSection": "latest",
    "genre": "Sci-Fi"
  },
  {
    "animeId": 29,
    "animeTitle": "Ninjala (TV)",
    "animeDesc": "The year is 20XX. The ninja, who once forged the history of Japan, were scattered across the country during the Meiji Restoration. As these ninja mingled with the other clans, their bloodline thinned, and they gradually faded from sight.",
    "animeImage": "/assets/images/l9.jpg",
    "animeSection": "latest",
    "genre": "Sci-Fi"
  },
  {
    "animeId": 30,
    "animeTitle": "The Legendary Hero Is Dead!",
    "animeDesc": "Far to the north of the world lies Hell's Gate, a portal formerly used by the Demon Lord to invade the human realm. Thanks to the legendary hero Shion Bladearts, wielder of Excalibu.",
    "animeImage": "/assets/images/l10.jpg",
    "animeSection": "latest",
    "genre": "Action"
  },
  {
    "animeId": 31,
    "animeTitle": "The Ancient Magus' Bride Season 2",
    "animeDesc": "The second season of Mahoutsukai no Yome. Chise was able to accept Elias and herself, if not necessarily everything about her situation. After Cartaphilus fell back into a slumber that would not last forever.",
    "animeImage": "/assets/images/l11.jpg",
    "animeSection": "latest",
    "genre": "Adventure"
  },
  {
    "animeId": 32,
    "animeTitle": "Dr. Stone: New World",
    "animeDesc": "With the Stone Wars over, the former members of Tsukasa's Empire of Might join forces with the Kingdom of Science to build a ship capable of sailing across open ocean to seek answers .",
    "animeImage": "/assets/images/l12.jpg",
    "animeSection": "latest",
    "genre": "Adventure"
  },
  {
    "animeId": 33,
    "animeTitle": "KamiKatsu: Working for God in a Godless World",
    "animeDesc": "Yukito's parents are the leaders of a cult. After he gets sacrificed, he gets reincarnated into another world where religion doesn't exist and porn books are akin to a child's doodles.",
    "animeImage": "/assets/images/l13.jpg",
    "animeSection": "latest",
    "genre": "Isekai"
  },
  {
    "animeId": 34,
    "animeTitle": "KonoSuba: An Explosion on This Wonderful World!",
    "animeDesc": "Crimson Magic Clan members Megumin and Yunyun are at the top of their class, but they still have a lot to learn. Yunyun's begun learning advanced magic, but Megumin has gone down a different path-the path of explosion magic!",
    "animeImage": "/assets/images/l14.jpg",
    "animeSection": "latest",
    "genre": "Fantasy"
  }
]