import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css'],
})
export class SliderComponent implements OnInit {

  spotlight: any;

  constructor(private service: ServiceService) {}

  ngOnInit(): void {

    //getting the anime data for spotlight section of the homepage
    this.service.getSpotlight().subscribe((data: any) => {
      this.spotlight = data;
      console.log(data);
    });
  }

}
