import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CollectorComponent } from './collector/collector.component';
import { AdminComponent } from './admin/admin.component';
import { ProfileComponent } from './profile/profile.component';
import { MobnumberComponent } from './mobnumber/mobnumber.component';
import { TakeotpComponent } from './takeotp/takeotp.component';
import { SetpasswordComponent } from './setpassword/setpassword.component';
import { CardviewComponent } from './cardview/cardview.component';
import { GenreComponent } from './genre/genre.component';
import { PaymentComponent } from './payment/payment.component';
import { AuthGuard } from './auth.guard';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { TakemailComponent } from './takemail/takemail.component';
import { EmailOtpComponent } from './email-otp/email-otp.component';
import { EPasswordComponent } from './e-password/e-password.component';
import { VideoplayerComponent } from './videoplayer/videoplayer.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  {path:"", component:LoginComponent},
  {path:"login", component:LoginComponent},
  {path:"register", component:RegisterComponent},
  {path:"collector",component:CollectorComponent},
  {path:"admin",canActivate:[AuthGuard],component:AdminComponent},
  {path:"profile",canActivate:[AuthGuard],component:ProfileComponent},
  {path:"mob",component:MobnumberComponent},
  {path:"takeotp",component:TakeotpComponent},
  {path:"setpassword",component:SetpasswordComponent},
  {path:"cardview",canActivate:[AuthGuard],component:CardviewComponent},
  {path:"genre",canActivate:[AuthGuard],component:GenreComponent},
  {path:"payment",canActivate:[AuthGuard],component:PaymentComponent},
  {path:"forgotpassword",canActivate:[AuthGuard],component:ForgotPasswordComponent},
  {path:"takeemail",canActivate:[AuthGuard],component:TakemailComponent},
  {path:"eotp",canActivate:[AuthGuard],component:EmailOtpComponent},
  {path:"epassword",canActivate:[AuthGuard],component:EPasswordComponent},
  {path:"vplayer",canActivate:[AuthGuard],component:VideoplayerComponent},
  {path:"search",canActivate:[AuthGuard],component:SearchComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
