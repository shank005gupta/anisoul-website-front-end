import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-videoplayer',
  templateUrl: './videoplayer.component.html',
  styleUrls: ['./videoplayer.component.css'],
})
export class VideoplayerComponent implements OnInit {

  epLink: any;

  constructor(
    private router: Router,
    private service: ServiceService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {

    // getting anime episode link and setting it to epLink
    this.route.queryParams.subscribe((episodeId: any) => {
      console.log(episodeId);
      this.epLink = episodeId.data;
    });
  }
}
