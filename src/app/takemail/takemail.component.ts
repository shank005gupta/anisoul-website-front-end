import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-takemail',
  templateUrl: './takemail.component.html',
  styleUrls: ['./takemail.component.css'],
})

export class TakemailComponent implements OnInit {

  flag:any;

  constructor(private router: Router, private service: ServiceService) {}

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  //method for taking email otp
  mob(eForm: any) {
    this.service.setMobile(eForm.emailId);
    this.service.getOtpByE(eForm.emailId).subscribe((data1: any) => {
      this.service.setData(data1);
      if (data1 != 0) {
        this.router.navigate(['eotp']);
      } else {
        this.flag=false;
        let msz = <HTMLElement>document.getElementById('popmsz');
        msz.innerHTML = 'Invalid Email-Address';
      }
    });
  }
}
