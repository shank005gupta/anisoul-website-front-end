import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/internal/Subject';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})

export class ServiceService {

  userLoginStatus: boolean;
  loginStatus: Subject<any>;
  id: any;
  otpValue: any;
  mobile: any;

  constructor(private http: HttpClient) {
    this.userLoginStatus = false;
    this.loginStatus = new Subject();
  }

  // for user logout authguard
  setUserLogout() {
    this.userLoginStatus = false;
    this.loginStatus.next(false);
  }

  // for user login authguard
  setUserLogin() {
    this.userLoginStatus = true;
    this.loginStatus.next(true);
  }

  //for getting user log status
  getUserLogStatus(): boolean {
    return this.userLoginStatus;
  }

  // for getting list of anime which have anime section as spotlight
  getSpotlight() {
    return this.http.get('/GetAnimeBySection/spotlight');
  }

  // for getting list of anime which have anime section as trending
  getTrending() {
    return this.http.get('/GetAnimeBySection/trending');
  }

  // for getting list of anime which have anime section as latest
  getLatest() {
    return this.http.get('/GetAnimeBySection/latest');
  }

  // for getting anime by animeId
  getCard(card: any) {
    return this.http.get('/GetAnimeById/' + card);
  }

  // getting all the anime with a specific genre
  getCardGenre(genre: any) {
    return this.http.get('/GetAnimeByGenre/' + genre);
  }

  //login user by userName
  getuser(loginForm: any) {
    return this.http.get('/getUserByUserName/' + loginForm.UserName);
  }

  // getting all the users registered on the site
  getAllUsers(): any {
    return this.http.get('http://localhost:8080/getAllUsers');
  }

  //for registering users
  register(users: any): any {
    return this.http.post('/registration', users);
  }

  // for updating profile of the user
  updateProfile(users: any): any {
    return this.http.put('/updateProfile', users);
  }

  // for donation to the website
  createOrder(order: any): Observable<any> {
    return this.http.post(
      'http://localhost:8080/pg/createOrder',
      {
        customerName: order.name,
        email: order.email,
        phoneNumber: order.phone,
        amount: order.amount,
      },
      httpOptions
    );
  }

  //checking if the user is registered or not
  userExist(loginForm: any): any {
    return this.http.get(
      '/login/' + loginForm.UserName + '/' + loginForm.password
    );
  }

  //adding data to the WatchList
  addList(WatchList: any): any {
    return this.http.post('/RegisterWatchList', WatchList);
  }

  //adding data to Favorite List
  favList(favList: any): any {
    return this.http.post('/RegisterFavorite', favList);
  }

  // setting userId to a variable
  setId(id: any) {
    this.id = id;
  }

  // getting userId
  getId() {
    return this.id;
  }

  // getting list of anime based on the userId WatchList
  getWatchList(userid: any) {
    return this.http.get('GetWatchListByUserId/' + userid);
  }

  // getting list of anime based on the userId for favourite
  getFavList(userId: any) {
    return this.http.get('/GetFavoriteByUserId/' + userId);
  }

  // deleting the data from the Watch List based on animeId
  deleteWatchList(animeId: any): any {
    return this.http.delete('/DeleteWatchList/' + animeId);
  }

  // deleting the data from the favourite List based on animeId
  deletefavList(animeId: any): any {
    return this.http.delete('/DeleteFavorite/' + animeId);
  }

  // for sending the otp based on mobile number
  getOtp(mobile: any): any {
    return this.http.get('/send-sms/' + mobile);
  }

  // for setting the value of otp
  setData(data: any) {
    this.otpValue = data;
  }

  // for getting the opt value
  getData(): any {
    return this.otpValue;
  }

  // for updating the profile data
  updatePass(setForm: any): any {
    return this.http.put('/updatePass', setForm);
  }

  // for setting mobile number
  setMobile(mobValue: any): any {
    return (this.mobile = mobValue);
  }

  // getting mobile number
  getMobile(): any {
    return this.mobile;
  }

  // for getting the otp from email
  getOtpByE(emailId: any): any {
    return this.http.get('/sendOTPByEmail/' + emailId);
  }

  // setting up a new password
  newpassword(setPass: any): any {
    return this.http.put('/updateNewPass', setPass);
  }

  // for getting all the anime episodes based on anime id
  getEp(anime: any) {
    return this.http.get('/GetEpisodeByAnimeId/' + anime);
  }

  // for getting all the anime regisetred
  getAll() {
    return this.http.get('/GetAllAnime');
  }

  // for registering an anime
  registerAnime(anime: any) {
    return this.http.post('/RegisterAnime', anime);
  }

  // for registering an anime episode
  registerEp(ep: any): any {
    return this.http.post('/RegisterEpisode', ep);
  }

  // for editing a specific anime data
  editAnime(anime: any) {
    return this.http.put('/UpdateAnime', anime);
  }

  // for deleting an anime using anime
  deleteAnimeById(animeId: any) {
    return this.http.delete('/DeleteAnime/' + animeId);
  }

  // for getting all episode
  getAllEpi() {
    return this.http.get('/GetAllEpisodes');
  }

  // for updating the episode info
  updateEpisode(episode: any) {
    return this.http.put('/UpdateEpisode', episode);
  }

  // for deleting a single episode using episodeID
  deleteEpisode(epId: any) {
    return this.http.delete('/DeleteEpisode/' + epId);
  }
}
