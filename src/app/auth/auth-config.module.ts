import { NgModule } from '@angular/core';
import { AuthModule } from 'angular-auth-oidc-client';
@NgModule({
    imports: [AuthModule.forRoot({
        config: {
            authority: 'https://dev-opkc0e38kr5obobb.us.auth0.com',
            redirectUrl: 'http://localhost:4200',
            postLogoutRedirectUri: window.location.origin,
            postLoginRoute:'/login',
            // postLoginRoute: window.location.origin,
            clientId: 'm7j4gtRSVbdFjVtnlHwPzxCyfLrB632p',
            scope: 'openid profile offline_access',
            responseType: 'code',
            silentRenew: true,
            useRefreshToken: true,
        }
      })],
    exports: [AuthModule],
})
export class AuthConfigModule {}