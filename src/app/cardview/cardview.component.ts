import { Component, OnInit } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cardview',
  templateUrl: './cardview.component.html',
  styleUrls: ['./cardview.component.css'],
})
export class CardviewComponent implements OnInit {

  cardId: any;
  card: any;
  watchlist: any;
  favList: any;
  user: any;
  episode: any;

  constructor(
    private router: Router,
    private service: ServiceService,
    private route: ActivatedRoute
  ) {
    this.watchlist = {
      watchId: 0,
      userId: 0,
      animeId: 0,
      animeTitle: '',
      animeDesc: '',
      animeImage: '',
      animeSection: '',
      Genre: '',
    };
    this.favList = {
      favouriteId: 0,
      userId: 0,
      animeId: 0,
      animeTitle: '',
      animeDesc: '',
      animeImage: '',
      animeSection: '',
      Genre: '',
    };
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((cardId: any) => {
      this.cardId = cardId.data;
    });

    // getting card data based on animeId
    this.service.getCard(this.cardId).subscribe((card: any) => {
      this.card = card;
      this.service.getEp(this.card.animeId).subscribe((data: any) => {
        this.episode = data;
      });
    });
  }

  //adding anime to WatchList
  addList() {
    this.user = localStorage.getItem('user');

    this.watchlist.userId = this.service.getId();
    this.watchlist.animeId = this.card.animeId;
    this.watchlist.animeTitle = this.card.animeTitle;
    this.watchlist.animeDesc = this.card.animeDesc;
    this.watchlist.animeImage = this.card.animeImage;
    this.watchlist.animeSection = this.card.animeSection;
    this.watchlist.genre = this.card.Genre;

    this.service.addList(this.watchlist).subscribe((data: any) => {
      if (data != null) {
        let msz = <HTMLElement>document.getElementById('msz');
        msz.innerHTML = 'Added to Watchlist';
      } else {
        let msz = <HTMLElement>document.getElementById('msz');
        msz.innerHTML = 'Already exists';
      }
    });
  }

  // adding anime to favorite List 
  addFav() {
    this.favList.userId = this.service.getId();
    this.favList.animeId = this.card.animeId;
    this.favList.animeTitle = this.card.animeTitle;
    this.favList.animeDesc = this.card.animeDesc;
    this.favList.animeImage = this.card.animeImage;
    this.favList.animeSection = this.card.animeSection;
    this.favList.genre = this.card.Genre;

    this.service.favList(this.favList).subscribe((data: any) => {
      console.log(this.favList);
      if (data != null) {
        let msz = <HTMLElement>document.getElementById('msz');
        msz.innerHTML = 'Added to Favorites';
      } else {
        let msz = <HTMLElement>document.getElementById('msz');
        msz.innerHTML = 'Already exists';
      }
    });
  }
  passlink(episodeLink: any) {
    this.router.navigate(['vplayer'], { queryParams: { data: episodeLink } });
  }
}
