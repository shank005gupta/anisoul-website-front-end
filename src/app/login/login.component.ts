import { Component, OnInit } from '@angular/core';
import { Observable, timeInterval } from 'rxjs';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { LoginResponse, OidcSecurityService } from 'angular-auth-oidc-client';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})

export class LoginComponent implements OnInit {
  isAuthenticated: boolean = false;
  data: any;
  profile:any;
  user:any;
  goglelogin:any;

  constructor(
    private router: Router,
    private service: ServiceService,
    private oidcSecurityservice: OidcSecurityService
  ) {}

  ngOnInit(): void {
    this.oidcSecurityservice.isAuthenticated$.subscribe(({ isAuthenticated }) => {
      this.isAuthenticated = isAuthenticated;
    })
    this.oidcSecurityservice.getUserData().subscribe((data)=> {
      this.user={fullName:data.name,emailId:data.nickname+'@gmail.com',phoneNo:"null",userName:data.given_name,password:"null"}
      this.profile={emailId:data.nickname+'@gmail.com',image:data.picture,fullName:data.name,userName:data.given_name,phoneNo:"Null"}
      localStorage.setItem("user", JSON.stringify(this.profile));
      this.goglelogin={UserName:data.given_name,password:'null'};
      
        //checking weather the user is exists in database or not 
      this.service.userExist(this.goglelogin).subscribe((isUserExist: boolean)=>{
        //getting the goole user login details and and setting the useid in service method
          this.service.getuser(this.goglelogin).subscribe((user) => {
              this.data = user;
              this.service.setId(this.data.userId);
          });
        if(!isUserExist)
        // registering the goole user in the data base 
        this.service.register(this.user).subscribe((user: any) => {
          this.data = user;
        this.service.setId(this.data.userId);}
        );
      });
    } );

   
    //if user is authenticated user will be sent to controller page 
    if (this.isAuthenticated) {
      this.service.setUserLogin();
      this.router.navigateByUrl('/collector');
    } 
    else {
      throw new Error('user not autenticated.');
      // this.router.navigate(['collector']);
    }
    
  }

  //google Login method
  googleLogin() {
    this.oidcSecurityservice.authorize();
    if (this.isAuthenticated) {
      this.service.setUserLogin();
      this.router.navigateByUrl('/collector');
    }
  }

  // method for logging in
  async login(loginForm: any) {
    if (loginForm.UserName === 'ADMIN' && loginForm.password === 'AniSoul') {
      this.service.setUserLogin();
      this.router.navigate(['admin']);
      this.service.setUserLogin();
    } else {
      this.service.setUserLogin();
      this.service.getuser(loginForm).subscribe((user) => {
        this.data = user;
        localStorage.setItem('user', JSON.stringify(user));
        this.service.setId(this.data.userId);
      });
      await this.service
        .userExist(loginForm)
        .subscribe((isUserExist: boolean) => {
          if (isUserExist) {
            this.router.navigate(['collector']);
          } else {
            let msz = <HTMLElement>document.getElementById('popmsz');
            msz.innerHTML = 'Invalid Credentials';
          }
          setTimeout(function () {
            let msz = <HTMLElement>document.getElementById('popmsz');
            msz.innerHTML = '';
          }, 2000);
        });
    }
  }
}
