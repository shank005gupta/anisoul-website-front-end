import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {

  result: any;

  constructor(
    private router: Router,
    private service: ServiceService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    
    // giving search result based on results object
    this.route.queryParams.subscribe((results: any) => {
      this.result = results['data'].map((item: string) => JSON.parse(item));
      console.log(this.result);
    });

    if (this.result == null) {
      let msz = <HTMLElement>document.getElementById('popmsz');
      msz.innerHTML = 'Failed to change password';
    }
  }

  //navigating to the cardview with card data
  sendCard(card: any) {
    this.router.navigate(['cardview'], { queryParams: { data: card } });
  }

}
