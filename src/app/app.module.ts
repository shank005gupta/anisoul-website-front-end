import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SliderComponent } from './slider/slider.component';
import { AdminComponent } from './admin/admin.component';
import { FooterComponent } from './footer/footer.component';
import { LatestComponent } from './latest/latest.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ShowUsersComponent } from './show-users/show-users.component';
import { TrendingComponent } from './trending/trending.component';
import { RouterModule } from '@angular/router';
import { CollectorComponent } from './collector/collector.component';
import { ProfileComponent } from './profile/profile.component';
import { VideoplayerComponent } from './videoplayer/videoplayer.component';
import { CardviewComponent } from './cardview/cardview.component';
import { MobnumberComponent } from './mobnumber/mobnumber.component';
import { TakeotpComponent } from './takeotp/takeotp.component';
import { SetpasswordComponent } from './setpassword/setpassword.component';
import { GenreComponent } from './genre/genre.component';
import { PaymentComponent } from './payment/payment.component';
import { TakemailComponent } from './takemail/takemail.component';
import { EmailOtpComponent } from './email-otp/email-otp.component';
import { EPasswordComponent } from './e-password/e-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SearchComponent } from './search/search.component';
import { AuthConfigModule } from './auth/auth-config.module';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SliderComponent,
    AdminComponent,
    FooterComponent,
    LatestComponent,
    RegisterComponent,
    LoginComponent,
    ShowUsersComponent,
    TrendingComponent,
    CollectorComponent,
    ProfileComponent,
    VideoplayerComponent,
    CardviewComponent,
    MobnumberComponent,
    TakeotpComponent,
    SetpasswordComponent,
    GenreComponent,
    PaymentComponent,
    TakemailComponent,
    EmailOtpComponent,
    EPasswordComponent,
    ForgotPasswordComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    AuthConfigModule
    ],
    
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
