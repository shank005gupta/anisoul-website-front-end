import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-e-password',
  templateUrl: './e-password.component.html',
  styleUrls: ['./e-password.component.css']
})
export class EPasswordComponent implements OnInit{

  setPass:any

  constructor(private router:Router,private service:ServiceService){
    this.setPass={
      emailId:"",
      password:""
    };

  }

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  //method for updatig password 
  pass(setForm:any){
    this.setPass.emailId=this.service.getMobile();
    
    this.setPass.password=setForm.pword;
  
    this.service.newpassword(this.setPass).subscribe((data1: any) => {
      alert("hii")
      if(data1!=0){
        this.router.navigate(['/']);
        
      }else{
        let msz = <HTMLElement>document.getElementById('popmsz');
        msz.innerHTML = 'Failed to change password';
      }
    });
       
  }
}
