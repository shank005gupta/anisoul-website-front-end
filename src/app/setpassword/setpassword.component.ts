import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-setpassword',
  templateUrl: './setpassword.component.html',
  styleUrls: ['./setpassword.component.css'],
})

export class SetpasswordComponent implements OnInit {

  setPass: any;

  constructor(private router: Router, private service: ServiceService) {
    this.setPass = {
      phoneNo: '',
      password: '',
    };
  }

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  // method for setting password
  pass(setForm: any) {
    this.setPass.phoneNo = this.service.getMobile();

    this.setPass.password = setForm.pword;
    this.service.updatePass(this.setPass).subscribe((data1: any) => {
      if (data1 != 0) {
        this.router.navigate(['/']);
      } else {
        let msz = <HTMLElement>document.getElementById('popmsz');
        msz.innerHTML = 'Failed to change password';
      }
    });
  }
  
}
