import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { Router } from '@angular/router';

declare var jQuery: any;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
})

export class AdminComponent implements OnInit {

  users: any;
  anime: any;
  episode: any;
  isUsers = false;
  isAnime = false;
  isAddAnime = false;
  isAddEpi = false;
  isEpi = false;
  animeObj: any;
  regAnime: any;
  editAnimeObj: any;
  regep: any;
  episodeObj: any;

  constructor(private service: ServiceService, private router: Router) {
    this.regAnime = {
      animeId: 0,
      animeTitle: '',
      animeDesc: '',
      animeImage: '',
      animeSection: '',
      genre: '',
    };

    this.animeObj = {
      animeId: 0,
      animeTitle: '',
      animeDesc: '',
      animeImage: '',
      animeSection: '',
      genre: '',
    };

    this.regep = {
      episodeNo: 0,
      episodeLink: '',
      type: '',
      anime: {
        animeId: 0,
        animeTitle: '',
        animeDesc: '',
        animeImage: '',
        animeSection: '',
        Genre: '',
      },
    };

    this.episodeObj = {
      episodeId: '',
      episodeNo: '',
      episodeLink: '',
      type: '',
      anime: { animeId: '' },
    };

  }

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  // getting all the users data and saving it in users object
  getAllUsers() {
    this.service.getAllUsers().subscribe((data: any) => {
      this.users = data;
    });
    this.isUsers = true;
    this.isAnime = false;
    this.isAddAnime = false;
    this.isAddEpi = false;
    this.isEpi = false;
  }

  //getting all the anime data 
  getAllAnime() {
    this.service.getAll().subscribe((data: any) => {
      this.anime = data;
    });

    //for hiding and showing the section
    this.isAnime = true;
    this.isUsers = false;
    this.isAddAnime = false;
    this.isAddEpi = false;
    this.isEpi = false;
  }

  //for hiding and showing the section
  showAnimeForm() {
    this.isAddAnime = true;
    this.isAnime = false;
    this.isUsers = false;
    this.isAddEpi = false;
    this.isEpi = false;
  }

  //for hiding and showing the section
  showEpiForm() {
    this.isUsers = false;
    this.isAnime = false;
    this.isAddAnime = false;
    this.isAddEpi = true;
    this.isEpi = false;
  }

  // getting all the episode data
  showEpi() {
    this.service.getAllEpi().subscribe((data: any) => {
      this.episode = data;
    });

    //for hiding and showing the section
    this.isUsers = false;
    this.isAnime = false;
    this.isAddAnime = false;
    this.isAddEpi = false;
    this.isEpi = true;
  }

  // method for adding anime
  addAnime(addAnimeForm: any) {
    this.regAnime.animeId = addAnimeForm.id;
    this.regAnime.animeTitle = addAnimeForm.title;
    this.regAnime.animeDesc = addAnimeForm.desc;
    this.regAnime.animeImage = addAnimeForm.link;
    this.regAnime.animeSection = addAnimeForm.section;
    this.regAnime.genre = addAnimeForm.genre;
    this.service.registerAnime(this.regAnime).subscribe((data1: any) => {
      if (data1 != null) {
        let msz = <HTMLElement>document.getElementById('msz');
        msz.innerHTML = 'Succesfully added';
        setTimeout(function () {
          let msz = <HTMLElement>document.getElementById('msz');
          msz.innerHTML = '';
        }, 2000);
      } else {
        let msz = <HTMLElement>document.getElementById('msz');
        msz.innerHTML = 'Invalid, try again with different fields';
        setTimeout(function () {
          let msz = <HTMLElement>document.getElementById('msz');
          msz.innerHTML = '';
        }, 2000);
      }
    });
    this.closeModal();
  }

  //method for adding episode
  addEpisode(form: any) {
    console.log(form);
    this.regep.episodeNo = form.number;
    this.regep.episodeLink = form.link;
    this.regep.type = form.type;
    this.regep.anime.animeId = form.aId;
    this.service.registerEp(this.regep).subscribe((data1: any) => {
      console.log(data1);
      if (data1 != null) {
        let msz = <HTMLElement>document.getElementById('mszEpi');
        msz.innerHTML = 'Succesfully added';
        setTimeout(function () {
          let msz = <HTMLElement>document.getElementById('mszEpi');
          msz.innerHTML = '';
        }, 2000);
      } else {
        let msz = <HTMLElement>document.getElementById('mszEpi');
        msz.innerHTML = 'Invalid, try again with different fields';
        setTimeout(function () {
          let msz = <HTMLElement>document.getElementById('mszEpi');
          msz.innerHTML = '';
        }, 2000);
      }
    });
    this.closeModal();
  }

  // modal hide and show methods for animeModal
  showModal(anime: any) {
    this.animeObj = anime;
    jQuery('#animeModal').modal('show');
  }

  closeModal() {
    jQuery('#animeModal').modal('hide');
  }

  //method for updating anime
  editAnime() {
    // console.log(this.animeObj);
    console.log(this.animeObj.animeId);
    this.service.editAnime(this.animeObj).subscribe((data1: any) => {
      if (data1 != null) {
        let msz = <HTMLElement>document.getElementById('eMsz');
        msz.innerHTML = 'Succesfully edited';
        setTimeout(function () {
          let msz = <HTMLElement>document.getElementById('eMsz');
          msz.innerHTML = '';
        }, 2000);
      } else {
        let msz = <HTMLElement>document.getElementById('eMsz');
        msz.innerHTML = 'Invalid, try again with different fields';
        setTimeout(function () {
          let msz = <HTMLElement>document.getElementById('eMsz');
          msz.innerHTML = '';
        }, 2000);
      }
    });
  }

  //method for deleting anime using animeId 
  delete(animeId: any) {
    // this.service.deleteAnimeById(animeId)
    this.service.deleteAnimeById(animeId).subscribe((data: any) => {
      const index = this.anime.findIndex((anime: any) => {
        return this.anime.animeId == animeId;
      });
      this.anime.splice(index, 1);
    });
  }

  // admin logout method
  logout() {
    this.service.setUserLogout();
    this.router.navigate(['']);
  }

  // modal show and hide method for episodeModal
  showModalEp(episode: any) {
    this.episodeObj = episode;
    jQuery('#episodeModal').modal('show');
  }

  closeEpiModal() {
    jQuery('#episodeModal').modal('hide');
  }

  // updating episode 
  editEpisode() {
    console.log(this.episodeObj);
    // console.log(this.animeObj);

    this.service.updateEpisode(this.episodeObj).subscribe((data1: any) => {
      console.log(data1);
      if (data1 != null) {
        let msz = <HTMLElement>document.getElementById('eMsz');
        msz.innerHTML = 'Succesfully edited';
        setTimeout(function () {
          let msz = <HTMLElement>document.getElementById('eMsz');
          msz.innerHTML = '';
        }, 2000);
      } else {
        let msz = <HTMLElement>document.getElementById('eMsz');
        msz.innerHTML = 'Invalid, try again with different fields';
        setTimeout(function () {
          let msz = <HTMLElement>document.getElementById('eMsz');
          msz.innerHTML = '';
        }, 2000);
      }
    });
  }

  // deleting episode using episodeId
  deleteEpi(epId: any) {
    this.service.deleteEpisode(epId).subscribe((data: any) => {
      const index = this.episode.findIndex((episode: any) => {
        return this.episode.episodeId == epId;
      });
      this.episode.splice(index, 1);
    });
  }
}
