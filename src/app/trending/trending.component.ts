import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trending',
  templateUrl: './trending.component.html',
  styleUrls: ['./trending.component.css'],
})

export class TrendingComponent implements OnInit {

  trending: any;

  constructor(private service: ServiceService, private router: Router) {}
  ngOnInit(): void {

    //getting the anime data for the trending section
    this.service.getTrending().subscribe((data: any) => {
      this.trending = data;
    });
  }

  //navigating to cardview with card data
  sendCard(card: any) {
    this.router.navigate(['cardview'], { queryParams: { data: card } });
  }

  //navigating to genre page with anime genre
  getGenre(arg0: string) {
    this.router.navigate(['genre'], { queryParams: { data: arg0 } });
  }
}
