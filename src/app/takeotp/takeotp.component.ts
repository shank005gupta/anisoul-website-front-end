import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MobnumberComponent } from '../mobnumber/mobnumber.component';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-takeotp',
  templateUrl: './takeotp.component.html',
  styleUrls: ['./takeotp.component.css'],
})

export class TakeotpComponent implements OnInit {

  constructor(private router: Router, private service: ServiceService) {}

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  //confirming password
  confirmpass(otpForm: any) {
    if (otpForm.otp == this.service.getData()) {
      this.router.navigate(['setpassword']);
    } else {
      let msz = <HTMLElement>document.getElementById('popmsz');
      msz.innerHTML = 'Invalid OTP';
    }
  }
  
}
