import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {

  user: any;
  flag: any;

  constructor(private service: ServiceService, private router: Router) {
    this.user = {
      fullName: '',
      emailId: '',
      phoneNo: '',
      userName: '',
      password: '',
    };
  }

  ngOnInit() {}

  // registering the user
  userReg(regForm: any) {
    this.user.fullName = regForm.fullName;
    this.user.emailId = regForm.emailId;
    this.user.phoneNo = regForm.phoneNo;
    this.user.userName = regForm.userName;
    this.user.password = regForm.password;
    console.log(this.user);
    this.service.register(this.user).subscribe((data1: any) => {
      if (data1 != null) {
        this.router.navigate(['login']);
      } else {
        this.flag=false;
        let msz = <HTMLElement>document.getElementById('msz');
        msz.innerHTML = 'Invalid, try different username or email';
        setTimeout(function () {
          let msz = <HTMLElement>document.getElementById('msz');
          msz.innerHTML = '';
        }, 3000);
      }
    });
  }
  
}
