import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { LoginResponse, OidcSecurityService } from 'angular-auth-oidc-client';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
 
export class ProfileComponent implements OnInit{
  user:any;
  profile:any;
  profile_img:any;
  data:any;
  userName:String="hello";
  watchList:any;
  favList:any;
  ProfileStatus = false;
  watchListStatus = false;
  favouriteStatus = false;

 

  constructor(private router: Router, private service: ServiceService, private oidcSecurityservice: OidcSecurityService){
  }
  ngOnInit(): void {
    this.profile={emailId:'',encodedpassword:'',fullName:'',password:'',phoneNo:'',userId:'',userName:''}
    this.userName= this.profile.userName;
    this.user = localStorage.getItem('user');
    this.profile = JSON.parse(this.user );
    this.profile_img=this.profile.image;
  
    this.service.getWatchList(this.service.getId()).subscribe((data:any)=>{
      this.watchList=data;
    });
    this.service.getFavList(this.service.getId()).subscribe((data: any) => {
      this.favList = data;
    });

    this.profile = {
      emailId: '',
      encodedpassword: '',
      fullName: '',
      password: '',
      phoneNo: '',
      userId: '',
      userName: '',
    };
    this.userName = this.profile.userName;
    this.user = localStorage.getItem('user');
    this.profile = JSON.parse(this.user);
    this.profile_img=this.profile.image;
  }

  //method for updating profile
  updateProfile() {
    this.service.updateProfile(this.profile).subscribe((data: any) => {});
  }

  //logout method
  logout() {
    this.service.setUserLogout();
    this.oidcSecurityservice.logoff().subscribe();
    this.router.navigate(['']);
  }

  // navigation to collector component
  homepage() {
    this.router.navigate(['collector']);
  }

  //navigation to cardview component with card data
  sendCard(card: any) {
    this.router.navigate(['cardview'], { queryParams: { data: card } });
  }

  // deeleting the watchlist card using watchId
  deleteCard(watchId: any) {
    this.service.deleteWatchList(watchId).subscribe((data: any) => {});
    this.service.getWatchList(this.service.getId()).subscribe((data: any) => {
      this.watchList = data;
    });
  }

  // deleting favorite card using forvoriteId
  deleteFavCard(favoriteId: any) {
    this.service.deletefavList(favoriteId).subscribe((data: any) => {});
    this.service.getFavList(this.service.getId()).subscribe((data: any) => {
      this.favList = data;
    });
  }
}
