import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from '../service.service';
declare var Razorpay: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css'],
})

export class PaymentComponent implements OnInit {

  title = 'demo';
  form: any = {};
  paymentId = '';
  error = '';

  constructor(private http: HttpClient, private orderService: ServiceService) {}

  ngOnInit() {}

  sayHello() {
    alert('Hello DK');
  }

  options = {
    key: '',
    amount: '',
    name: 'Anisoul',
    description: 'Anime Streaming platform',
    image: '/assets/images/AnimeSprint_adobe_express.svg',
    order_id: '',
    handler: function (response: any) {
      var event = new CustomEvent('payment.success', {
        detail: response,
        bubbles: true,
        cancelable: true,
      });
      window.dispatchEvent(event);
    },
    prefill: {
      name: '',
      email: '',
      contact: '',
    },
    notes: {
      address: '',
    },
    theme: {
      color: '#3399cc',
    },
  };

  onSubmit(): void {
    this.paymentId = '';
    this.error = '';
    this.orderService.createOrder(this.form).subscribe(
      (data) => {
        this.options.key = data.secretId;
        this.options.order_id = data.razorpayOrderId;
        this.options.amount = data.applicationFee; //paise
        this.options.prefill.name = 'Anisoul';
        this.options.prefill.email = 'aniSoul@gmail.com';
        this.options.prefill.contact = '999999999';

        if (data.pgName === 'razor1') {
          var rzp1 = new Razorpay(this.options);
          rzp1.open();
        }

        rzp1.on('payment.failed', (response: any) => {
          console.log(response);
          console.log(response.error.code);
          console.log(response.error.description);
          console.log(response.error.source);
          console.log(response.error.step);
          console.log(response.error.reason);
          console.log(response.error.metadata.order_id);
          console.log(response.error.metadata.payment_id);
          this.error = response.error.reason;
        });
      },
      (err) => {
        this.error = err.error.message;
      }
    );
  }
  @HostListener('window:payment.success', ['$event'])
  onPaymentSuccess(event: any): void {
    console.log(event.detail);
  }
}
