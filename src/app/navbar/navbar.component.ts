import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {

  searchQuery: string = '';
  contents: any;
  results: any;

  constructor(private router: Router, private service: ServiceService) {
    this.results = [];
  }

  ngOnInit(): void {

    // getting all anime and setting it as contents
    this.service.getAll().subscribe((data: any) => {
      this.contents = data;
    });

  }

  // navigating to profile component
  profilenav() {
    this.router.navigate(['profile']);
  }
  // navigating to collector component
  homepage() {
    this.router.navigate(['collector']);
  }

  // method for search box
  async search() {
    for (let item of this.contents) {
      if (
        item.animeTitle.toLowerCase().includes(this.searchQuery.toLowerCase())
      ) {
        this.results.push(JSON.stringify(item));
        console.log(item);
      }
    }
    await this.router.navigate(['search'], {
      queryParams: { data: this.results },
    });
  }
  
}
