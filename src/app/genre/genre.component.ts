import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.css'],
})
export class GenreComponent implements OnInit {

  genre: any;
  card: any;

  constructor(
    private router: Router,
    private service: ServiceService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((genre: any) => {
      this.genre = genre.data;
    });

    //getting card based on genre
    this.service.getCardGenre(this.genre).subscribe((card: any) => {
      this.card = card;
    });
  }

  //navigating to cardview with card data
  sendCard(card: any) {
    this.router.navigate(['cardview'], { queryParams: { data: card } });
  }
  
}
