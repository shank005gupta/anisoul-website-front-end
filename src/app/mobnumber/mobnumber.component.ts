import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-mobnumber',
  templateUrl: './mobnumber.component.html',
  styleUrls: ['./mobnumber.component.css'],
})

export class MobnumberComponent implements OnInit {

  constructor(private router: Router, private service: ServiceService) {}

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  // checking for valid mobile number and navigating to the next page if its correct
  mob(mobForm: any) {
    this.service.setMobile(mobForm.mobile);
    this.service.getOtp(mobForm.mobile).subscribe((data1: any) => {
      this.service.setData(data1);
      if (data1 != 0) {
        this.router.navigate(['takeotp']);
      } else {
        let msz = <HTMLElement>document.getElementById('popmsz');
        msz.innerHTML = 'Invalid Mobile Number';
      }
    });
  }
  
}
